#!/bin/bash

curl -X POST -H "Content-Type: application/json" --data '{
  "name": "mongodb-connector",
  "config": {
    "connector.class": "io.debezium.connector.mongodb.MongoDbConnector",
    "mongodb.connection.string": "mongodb://mongodb-for-debezium:27017/?replicaSet=rs0",
    "mongodb.hosts": "rs0/mongodb-for-debezium:27017",
    "database.whitelist": "testdb",
    "mongodb.members.auto.discover": "false",
    "snapshot.mode": "initial",
    "topic.prefix": "mongo_db",
    "key.converter": "org.apache.kafka.connect.json.JsonConverter",
    "value.converter": "org.apache.kafka.connect.json.JsonConverter",
    "key.converter.schemas.enable": "false",
    "value.converter.schemas.enable": "false"
  }
}' http://localhost:8083/connectors

// Check the status of the connector
curl http://localhost:8083/connectors/mongodb-connector/status