const { MongoClient } = require("mongodb");
const crypto = require("crypto");

const uri = "mongodb://mongodb-for-debezium:27017/testdb?replicaSet=rs0";

const client = new MongoClient(uri);

const collectionName = "testCollection";

function getRandomRecord(i) {
  return {
    name: `name-${i}`,
    age: 30 + +i,
    address: `address-${i}`,
    [crypto.randomUUID()]: "random",
  };
}

function createNRecords(n, collection) {
  const records = [];
  for (let i = 0; i < n; i++) {
    records.push(getRandomRecord(i));
  }

  return collection.insertMany(records);
}

async function run() {
  try {
    await client.connect();
    console.log("Connected successfully to server");

    const db = client.db("testdb");
    const collection = db.collection(collectionName);
    const insertResult = await createNRecords(50, collection);
    console.log("Inserted documents:", insertResult.insertedCount);

    const documents = await collection.find({}).toArray();
    console.log("Documents in collection:", documents);
  } catch (err) {
    console.error("An error occurred:", err);
  } finally {
    await client.close();
  }
}

run().catch(console.error);
