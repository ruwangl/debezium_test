const { Kafka } = require("kafkajs");
const crypto = require("crypto");

const kafka = new Kafka({
  clientId: "my-app",
  brokers: ["kafka-for-debezium:29092"],
});

const id = crypto.randomUUID();
const groupId = 'current-group';
const consumer = kafka.consumer({ groupId });

const run = async () => {
  console.log(`[${id}]Connecting to Kafka broker`);
  await consumer.connect();

  console.log(`[${id}]Connected to Kafka broker`);

  await consumer.subscribe({
    topic: "mongo_db.testdb.testCollection",
    fromBeginning: true,
  });

  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      console.log({
        partition,
        topic,
        offset: message.offset,
        value: message.value.toString(),
      });
    },
  });
};

run().catch(console.error);

const signalTraps = ["SIGTERM", "SIGINT", "SIGUSR2"];
signalTraps.forEach((type) => {
  process.once(type, async () => {
    try {
      await consumer.disconnect();
    } finally {
      process.kill(process.pid, type);
    }
  });
});
