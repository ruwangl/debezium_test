## Run docker-compose file
```bash
docker-compose up
```

## Prepare the environment
### Connect to MongoDB and run following command
- This has to be done only once
- If you don't have local mongo db replica set
- use following connection string
```
mongodb://localhost:27017/
```
- Run this to set the replica set
```bash
rs.initiate()
```

### Then run following command create mongo connector to debezium
- **Only for first time**
```bash
./init-mongo-in-debazium.sh
```

## After running the above command
### Connect to kafka-consumer-nodejs container and run following command to see the consumer logs
```bash
docker exec -it kafka-consumer-nodejs-debezium /bin/bash

# To  install the dependencies
npm i
# To consume the events from kafka
node consumer.js
```

### Connect to mongo-populate-nodejs container and run following command to populate the data
```bash
docker exec -it mongo-populate-nodejs-debezium /bin/bash
# To  install the dependencies
npm i

# to populate the data
node populate.js
```

### Run kafka scripts - if needed
```bash
docker exec -it kafka-for-debezium bash

cd /usr/bin/
./kafka-topics --bootstrap-server kafka-for-debezium:9092 --list
```